/*  main.cpp
 * 
    The program this file is part of is free software: 
    you can redistribute it and/or modify it under the terms 
    of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    this program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 *  Copyright 2021 Alberto Ossola
 *  alberto.ossola1@studenti.unimi.it
*/

#include <A4988.h>
#include "Reader.h"

const int PhotodiodePin = A0;

Reader* reader;

int readPhotodiode();

void setup(){ 
    Serial.begin(9600); 

    reader = new Reader(
        new A4988(200, 8, 9, 10, 11, 12),
        readPhotodiode
    );

    reader->loadCalibrationFromEEPROM();
}

int readPhotodiode(){
    return analogRead(PhotodiodePin);
}

void loop(){
    if(Serial.available()){
        String command = Serial.readStringUntil('\n');

        Serial.println(command);

        if(command.equals("calibrate leds")){
            reader->calibrateLeds();
            return;
        }

        if(command.equals("calibrate row length")){
            reader->calibrateRowWidth();
            Serial.println("Row length calibrated");

            return;
        }

        if(command.equals("calibrate border length")){
            reader->calibrateBorderLength();
            Serial.println("Border length calibrated");

            return;
        }

        if(command.equals("autocalibrate")){
            reader->autoCalibrate();
            Serial.println("Border length calibrated");

            return;
        }

        if(command.equals("save calibration")){
            reader->saveCalibrationToEEPROM();

            Serial.println("Calibration saved");

            return;
        }

        if(command.equals("load calibration")){
            reader->loadCalibrationFromEEPROM();

            Serial.println("Calibration loaded");

            return;
        }

        if(command.equals("read row")){
            Serial.println(reader->readRow());
            return;
        }

        if(command.equals("load card")){
            reader->loadCard();

            return;
        }

        if(command.equals("read card")){
            char buf[80] = {0};
            reader->readCard(buf);
            Serial.println(buf);
        }

        if(command.equals("advance")){
            reader->advance();
        }

        if(command.equals("eject")){
            reader->ejectCard();
        }

        if(command.equals("show settings")){
            for(int i = 0; i < sizeof(reader->settings) / sizeof(Setting); i++){
                Serial.print(reader->settings[i].name);
                Serial.print(": ");
                Serial.println(reader->settings[i].value);
            }
        }

        if(command.equals("edit setting")){
            for(int i = 0; i < sizeof(reader->settings) / sizeof(Setting); i++){
                Serial.print(i);
                Serial.print(") ");
                Serial.print(reader->settings[i].name);
                Serial.print(": ");
                Serial.println(reader->settings[i].value);
            }

            Serial.println("Select a setting: ");

            while(!Serial.available());
            delay(300);
            int selection = Serial.parseInt();

            //Flush serial
            while(Serial.available()){
                Serial.read();
            }

            if(selection >= 0 && selection < (sizeof(reader->settings) / sizeof(Setting))){
                Serial.println("Enter the new value: ");

                while(!Serial.available());
                delay(300);

                int newValue = Serial.parseInt();

                //Flush serial
                while(Serial.available()){
                    Serial.read();
                }

                reader->settings[selection].value = newValue;

                Serial.println("Ok");
            }
        }
    }
}