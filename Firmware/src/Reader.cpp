/*  Reader.cpp
 * 
    The program this file is part of is free software: 
    you can redistribute it and/or modify it under the terms 
    of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    this program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 *  Copyright 2021 Alberto Ossola
 *  alberto.ossola1@studenti.unimi.it
*/

#include "Reader.h"
#include "Code.h"

#include "FastLED.h"
#include "A4988.h"
#include "EEPROM.h"

Reader::Reader(A4988* stepper, int (*readPhotodiode)()){
    this->stepper = stepper;
    this->readPhotodiode = readPhotodiode;

    this->ledColors = new CRGB[12];

    loadCalibrationFromEEPROM();

    FastLED.addLeds<NEOPIXEL, A5>(this->ledColors, 12);
    FastLED.clear();

    this->stepper->begin(this->settings[StepperSpeed].value, this->settings[StepperSpeed].value);
    this->stepper->disable();
}

void Reader::calibrateLeds(){
    this->ejectCard();

    for(int i = 0; i < FastLED.size(); i++){
        //Adjust the led lower bound
        FastLED.clear();
        FastLED.show();

        delay(this->settings[DwellTime].value);

        this->levels[i].low = this->readPhotodiode();

        //Adjust the led for full brightness

        FastLED.leds()[i].setRGB(255, 255, 255);
        FastLED.show();

        delay(this->settings[DwellTime].value);

        this->levels[i].high = this->readPhotodiode();

        char buf[128];

        sprintf(buf, "Values read for column %d: Low: %d, High: %d\n", i, levels[i].low, levels[i].high);

        Serial.print(buf);
    }

    FastLED.clear();
    FastLED.show();
}

int Reader::manualMovement(int stepsAtTime){
    int steps = 0;

    while(true){
        if(Serial.available()){
            switch(Serial.read()){
                case 'f':
                    this->stepper->move(stepsAtTime);
                    steps += stepsAtTime;
                    break;

                case 'b':
                    this->stepper->move(-stepsAtTime);
                    steps -= stepsAtTime;
                    break;

                case 'a':
                    return -1;
                    break;

                case 'o':
                    return steps;
                    break;
            }
        }
    }
}

void Reader::calibrateBorderLength(){
    this->loadCard();

    Serial.println("'f': forward, 'b': back. Align the edge of the card with some reference point.");
    Serial.println("Press 'o' to confirm, 'a' to abort");

    this->stepper->enable();
    this->stepper->setRPM(this->settings[StepperSpeed].value);
    this->stepper->setMicrostep(this->settings[StepperMicrosteps].value);
    
    if(manualMovement(4) == -1){
        return;
    }

    Serial.println("'f': forward, 'b': back. Move until the reader has advanced as long as the border width.");
    Serial.println("Press 'o' to confirm, 'a' to abort");

    int steps = manualMovement(4);

    if(steps == -1){
        return;
    }

    this->settings[BorderWidth].value = steps;
}

void Reader::calibrateRowWidth(){
    this->loadCard();

    Serial.println("'f': forward, 'b': back. Align a row with some reference point.");
    Serial.println("Press 'o' to confirm, 'a' to abort");

    this->stepper->enable();
    this->stepper->setRPM(this->settings[StepperSpeed].value);
    this->stepper->setMicrostep(this->settings[StepperMicrosteps].value);
    
    if(manualMovement(4) == -1){
        return;
    }

    Serial.println("'f': forward, 'b': back. Move until the reader has advanced 5 rows.");
    Serial.println("Press 'o' to confirm, 'a' to abort");

    int steps = manualMovement(4);

    Serial.println(steps);

    if(steps == -1){
        return;
    }

    this->settings[RowWidth].value = steps / 5;
}

void Reader::ejectCard(){
    this->stepper->enable();
    this->stepper->setRPM(this->settings[StepperSpeed].value);
    this->stepper->setMicrostep(this->settings[StepperMicrosteps].value);

    this->stepper->move(this->settings[RowWidth].value * this->settings[RowsPerCard].value + 10);
    this->stepper->disable();
}

void Reader::loadCard(){
    this->stepper->enable();
    this->stepper->setRPM(this->settings[StepperSpeed].value);
    this->stepper->setMicrostep(this->settings[StepperMicrosteps].value);

    int stepsToMove = 20000;
    this->stepper->startMove(stepsToMove);

    FastLED.clear();
    this->ledColors[5].setRGB(255, 255, 255);
    FastLED.show();

    delay(this->settings[DwellTime].value);

    int threshold = (this->levels[5].high + this->levels[5].low) / 2;

    while(this->stepper->getStepsRemaining()){
        if(this->stepper->getStepsRemaining() % 2 == 0)
            if(readPhotodiode() < threshold){
                break;
            }

        int interval = this->stepper->nextAction();

        delayMicroseconds(interval);
    }

    this->stepper->stop();

    //Move from border to first row
    this->stepper->move(this->settings[BorderWidth].value);

    this->stepper->disable();

    FastLED.clear(true);
}

uint8_t Reader::readColumn(int column){
    FastLED.clear();
    FastLED.leds()[column].setRGB(255, 255, 255);
    FastLED.show();

    delay(this->settings[DwellTime].value);

    int threshold = (this->levels[column].high + this->levels[column].low) / 2;

    int value = readPhotodiode();

    char buf[64];
    sprintf(buf, "Threshold: %d, value: %d", threshold, value);
    Serial.println(buf);

    if(value > threshold){
        return 1;
    }
    else{
        return 0;
    }
}

char Reader::readRow(){
    uint16_t row = 0;

    for(int i = 0; i < 12; i++){
        row |= (readColumn(i) << i);
    }

    for(uint16_t i = 0; i < (sizeof(IBM029) / sizeof(CodeEntry)); i++){
        if(row == IBM029[i].bitmap){
            return IBM029[i].character;
        }
    }

    return '.';
}

void Reader::autoCalibrate(){
    ejectCard();

    calibrateLeds();

    loadCard();

    int offsetSteps = 0;
    int onRowSteps = 0;

    this->stepper->enable();

    while(offsetSteps < 400){
        if(readRow() == 'q'){
            break;
        }

        this->stepper->move(8);
        offsetSteps += 8;
    }

    if(offsetSteps >= 400){
        Serial.println("Failed");
        return;
    }


    while(onRowSteps < 250){
        if(readRow() != 'q'){
            break;
        }

        this->stepper->move(8);
        onRowSteps += 8;
    }

    this->settings[BorderWidth].value = offsetSteps + (onRowSteps / 2);

    this->stepper->disable();
}

void Reader::readCard(char* buffer){
    loadCard();

    for(uint16_t i = 0; i < this->settings[RowsPerCard].value; i++){
        buffer[i] = this->readRow();

        this->advance();
    }

    ejectCard();
}

void Reader::advance(int16_t rows){
    this->stepper->enable();
    this->stepper->setRPM(this->settings[StepperSpeed].value);
    this->stepper->setMicrostep(this->settings[StepperMicrosteps].value);

    this->stepper->move(this->settings[RowWidth].value * rows);

    this->stepper->disable();
}

void Reader::loadCalibrationFromEEPROM(){
    EEPROM.begin();

    for(int i = 0; i < sizeof(this->settings) / sizeof(Setting); i++){
        EEPROM.get(i * sizeof(int16_t), this->settings[i].value);
    }

    EEPROM.end();
}

void Reader::saveCalibrationToEEPROM(){
    EEPROM.begin();

    for(int i = 0; i < sizeof(this->settings) / sizeof(Setting); i++){
        EEPROM.put(i * sizeof(int16_t), this->settings[i].value);
    }

    EEPROM.end();
}