/*  Code.h
 * 
    The program this file is part of is free software: 
    you can redistribute it and/or modify it under the terms 
    of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    this program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 *  Copyright 2021 Alberto Ossola
 *  alberto.ossola1@studenti.unimi.it
*/

#include "stdint.h"

struct CodeEntry {
    char character;
    uint16_t bitmap;
};

const CodeEntry IBM029[] = {
    {'&', 0b100000000000},
    {'-', 0b010000000000},
    {'0', 0b001000000000},
    {'1', 0b000100000000},
    {'2', 0b000010000000},
    {'3', 0b000001000000},
    {'4', 0b000000100000},
    {'5', 0b000000010000},
    {'6', 0b000000001000},
    {'7', 0b000000000100},
    {'8', 0b000000000010},
    {'9', 0b000000000001},

    {'a', 0b100100000000},
    {'b', 0b100010000000},
    {'c', 0b100001000000},
    {'d', 0b100000100000},
    {'e', 0b100000010000},
    {'f', 0b100000001000},
    {'g', 0b100000000100},
    {'h', 0b100000000010},
    {'i', 0b100000000001},

    {'j', 0b010100000000},
    {'k', 0b010010000000},
    {'l', 0b010001000000},
    {'m', 0b010000100000},
    {'n', 0b010000010000},
    {'o', 0b010000001000},
    {'p', 0b010000000100},
    {'q', 0b010000000010},
    {'r', 0b010000000001},

    {'/', 0b001100000000},
    {'s', 0b001010000000},
    {'t', 0b001001000000},
    {'u', 0b001000100000},
    {'v', 0b001000010000},
    {'w', 0b001000001000},
    {'x', 0b001000000100},
    {'y', 0b001000000010},
    {'z', 0b001000000001},

    {' ', 0b000000000000},

    {':', 0b000010000010},
    {'#', 0b000001000010},
    {'@', 0b000000100010},
    {'\'',0b000000010010},
    {'=', 0b000000001010},
    {'"', 0b000000000110},

    {'`', 0b100010000010},
    {'.', 0b100001000010},
    {'<', 0b100000100010},
    {'(', 0b100000010010},
    {'+', 0b100000001010},
    {'|', 0b100000000110},

    {'!', 0b010010000010},
    {'$', 0b010001000010},
    {'*', 0b010000100010},
    {')', 0b010000010010},
    {';', 0b010000001010},
    {'^', 0b010000000110},

    {'~', 0b001010000010},
    {',', 0b001001000010},
    {'%', 0b001000100010},
    {'_', 0b001000010010},
    {'>', 0b001000001010},
    {'?', 0b001000000110},
};