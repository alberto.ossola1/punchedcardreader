/*  Reader.h
 * 
    The program this file is part of is free software: 
    you can redistribute it and/or modify it under the terms 
    of the GNU General Public License as published by
    the FreLengthe Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    this program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 *  Copyright 2021 Alberto Ossola
 *  alberto.ossola1@studenti.unimi.it
*/

#include "stdint.h"
#include "Setting.h"

class A4988;
class CFastLED;
class CRGB;

struct LedLevels{
    int low;
    int high;
};

class Reader{
private:
    A4988* stepper;
    int (*readPhotodiode)();

    LedLevels levels[12]; 

    CRGB* ledColors;

    int manualMovement(int);

public:
    enum SettingIndex {
        RowWidth,
        BorderWidth,
        DwellTime,
        StepperSpeed,
        StepperMicrosteps,
        ThresholdNumerator,
        ThresholdDenominator,
        RowsPerCard
    };

    Setting settings[8] = {
        {"Row Width", 122},
        {"Border Width", 252}, 
        {"Dwell Time", 50},
        {"Stepper Speed", 60},
        {"Stepper Microsteps", 16},
        {"Threshold Numerator", 2},
        {"Threshold Denominator", 5},
        {"Rows per card", 80}
    };

    Reader(A4988*, int(*)());

    void advance(int16_t rows = 1);

    uint8_t readColumn(int);

    char readRow();

    void readCard(char* buffer);

    void loadCard();

    void ejectCard();

    void calibrateLeds();

    void calibrateRowWidth();
    void calibrateBorderLength();

    void autoCalibrate();

    void loadCalibrationFromEEPROM();
    void saveCalibrationToEEPROM();
};